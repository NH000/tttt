# INSTALL COMMANDS
INSTALL := install
INSTALL_PROGRAM := $(INSTALL)
INSTALL_DATA := $(INSTALL) -m 644

# LIBRARIES
INCLUDES := $(shell pkg-config --cflags libutf8proc)
LIBS := -lm -lpthread $(shell pkg-config --libs libutf8proc)

# USER MODIFIABLE VARIABLES
PROG := tttt
DEBUG :=
PROFILE :=
OPTIMIZE :=
CFLAGS := -Wall -c
LDFLAGS :=
SRCDIR := src
OBJDIR := obj
PODIR := po
DEPSDIR := .deps
MANDIR := man
INSTALLDEST := /usr/bin
LOCALEDEST := /usr/share/locale
MANDEST := /usr/share/man

# USER NON MODIFIABLE VARIABLES
SRCS := $(wildcard $(SRCDIR)/*.c)
OBJS := $(patsubst $(SRCDIR)/%.c,$(OBJDIR)/%.o,$(SRCS))
SWITCHES := -DTTTT_GETTEXT_TEXT_DOMAIN="\"$(PROG)\"" -DTTTT_GETTEXT_TEXT_DOMAIN_DIR="\"$(subst ",\",$(LOCALEDEST))\""

# ENABLE DEBUG
ifdef DEBUG
    CFLAGS := $(CFLAGS) -g
endif

# ENABLE PROFILE
ifdef PROFILE
    CFLAGS := -pg $(CFLAGS)
    LDFLAGS := -pg $(LDFLAGS)
endif

# ENABLE OPTIMIZE
ifdef OPTIMIZE
    CFLAGS := -O3 $(CFLAGS)
endif

# FUNCTIONS

# Deletes file $(2) from directory $(1), then deletes empty parent directories subsequently.
delete_file_and_empty_dirs = if test -d '$(1)'; then rm -f '$(1)/$(2)' && rmdir -p --ignore-fail-on-non-empty '$(1)'; fi

# Generates MO file into $(LOCALEDEST) for language code $(1).
generate_mo = mkdir -p '$(LOCALEDEST)/$(1)/LC_MESSAGES' && msgfmt -o '$(LOCALEDEST)/$(1)/LC_MESSAGES/$(PROG).mo' '$(PODIR)/$(1).po'

# Delete MO file from $(LOCALEDEST) given language code as $(1).
# Then delete all of its parent directories in bottom-to-top order if they are empty.
uninstall_mo = $(call delete_file_and_empty_dirs,$(LOCALEDEST)/$(1)/LC_MESSAGES,$(PROG).mo)

# Recursively installs manual pages to subdirectory $(1) of $(MANDEST).
install_man = $(foreach f,$(notdir $(wildcard $(MANDIR)/$(1)/*)),$(if $(shell test -d '$(MANDIR)/$(1)/$(f)' && echo "dir"),$(call install_man,$(1)/$(f)),$(INSTALL_DATA) -D '$(MANDIR)/$(1)/$(f)' '$(MANDEST)/$(1)/$(PROG).6' && sed -i 's/\bTTTT_EXEC_NAME\b/$(PROG)/g' '$(MANDEST)/$(1)/$(PROG).6';))

# Recursively removes installed manual pages from subdirectory $(1) of $(MANDEST).
uninstall_man = $(foreach f,$(notdir $(wildcard $(MANDIR)/$(1)/*)),$(if $(shell test -d '$(MANDIR)/$(1)/$(f)' && echo "dir"),$(call uninstall_man,$(1)/$(f)),$(call delete_file_and_empty_dirs,$(MANDEST)/$(1),$(PROG).6);))

# DEPENDENCIES
include $(wildcard $(DEPSDIR)/*.d)

# RULES

# PHONY TARGETS
.PHONY: help all deps clean install install-man uninstall uninstall-man

# HELP MENU
help:
	$(info VARIABLES)
	$(info ==================================================================)
	$(info PROG:        Name of the executable.)
	$(info DEBUG:       Embed debugging information.)
	$(info PROFILE:     Embed profiling information.)
	$(info OPTIMIZE:    Enable compiler optimizations.)
	$(info CC:          Compiler to use.)
	$(info CFLAGS:      Compiler flags.)
	$(info LDFLAGS:     Linker flags.)
	$(info SRCDIR:      Source directory.)
	$(info OBJDIR:      Object directory.)
	$(info PODIR:       PO directory.)
	$(info DEPSDIR:     Dependency information directory.)
	$(info MANDIR:      Manual pages directory.)
	$(info INSTALLDEST: Executable installation directory.)
	$(info LOCALEDEST:  Locale installation directory.)
	$(info MANDEST:     Manual pages installation directory.)
	$(info )
	$(info RULES)
	$(info ==================================================================)
	$(info help:          Display this help menu.)
	$(info deps:          Create dependency files needed for building.)
	$(info all:           Build object files and link them into executable.)
	$(info clean:         Remove files produced by the building processs.)
	$(info install:       Install executable and its assets.)
	$(info install-man:   Install manual pages.)
	$(info uninstall:     Uninstall executable and its assets.)
	$(info uninstall-man: Uninstall manual pages.)

# Directory creator.
$(OBJDIR) $(DEPSDIR):
	@mkdir -p '$@'

deps: $(SRCS) | $(DEPSDIR)
	$(info Building dependency information...)
	@$(foreach src,$(SRCS),$(CC) $(SWITCHES) -MT $(patsubst $(SRCDIR)/%.c,'$(OBJDIR)/%.o',$(src)) -MM -MF $(patsubst $(SRCDIR)/%.c,'$(DEPSDIR)/%.d',$(src)) '$(src)';)

all: $(PROG)

$(PROG): $(OBJS)
	$(CC) $(LDFLAGS) -o '$(PROG)' $(patsubst %,'%',$(OBJS)) $(LIBS)

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)
	$(CC) $(SWITCHES) $(CFLAGS) -o '$@' '$<' $(INCLUDES)

clean:
	$(info Cleaning directory...)
	@rm -rf '$(OBJDIR)' '$(PROG)' '$(DEPSDIR)'

install: $(PROG) $(PODIR)/*.po
	$(INSTALL_PROGRAM) -D -t '$(INSTALLDEST)' '$(PROG)'
	$(foreach po,$(wildcard $(PODIR)/*.po),$(call generate_mo,$(patsubst $(PODIR)/%.po,%,$(po)));)

install-man: $(MANDIR)
	$(call install_man)

uninstall:
	rm -f '$(INSTALLDEST)/$(PROG)'
	$(foreach po,$(wildcard $(PODIR)/*.po),$(call uninstall_mo,$(patsubst $(PODIR)/%.po,%,$(po)));)

uninstall-man:
	$(call uninstall_man)
