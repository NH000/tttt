# tttt

## Description
Play tic-tac-toe in your terminal.

+ Round limited mode.
+ Time limited mode.
+ Vi-like keybindings.

## Requirements
+ cc
+ utf8proc
+ pthread
+ gettext
+ make
+ sh
+ pkgconfig
+ coreutils
+ msgfmt
+ sed (needed only for installing manual)

GCC and Clang are guaranteed to compile the project.

## Installation
Enter the project's root directory and run the following commands to compile and install the program:

```
make all OPTIMIZE=1 # Compile program with optimizations.
make install        # Install the program and its assets.
```

To install manual pages run:

```
make install-man
```

To see more make options (such as installation location, etc.), run `make help`.

## Uninstallation
To uninstall the program and its assets go into the project's root directory and run `make uninstall`.
Or if you want to uninstall manual pages instead, run `make uninstall-man`.
Whichever the case, make sure that appropriate make variables are set to values that were used during the installation.

## Translations
Default language of this program (in effect when `C` or `POSIX` is set as locale) is US English.  
The following translations are available:

| **Language**       | **Translator**                                           | **For versions** |
|--------------------|----------------------------------------------------------|------------------|
| Serbian (Cyrillic) | [Nikola Hadžić](mailto:nikola.hadzic.000@protonmail.com) | 1.0              |
| Serbian (Latin)    | [Nikola Hadžić](mailto:nikola.hadzic.000@protonmail.com) | 1.0              |

### Translation process
This program is written to be easily translatable into multiple languages, and it achieves that through the use of [`gettext`](https://www.gnu.org/software/gettext/) library.
Translations are located in `po` directory. Files in that directory contain translations, each PO file corresponding to one locale.

To add a new or update existing translation, enter the project's `po` directory and run the following command:

```
make %.po  # Generate/update PO file; "%" should be replaced with a language code.
```

Afterwards, you can edit created/updated PO file (see [`gettext` manual](https://www.gnu.org/software/gettext/manual/gettext.html) for details),
translating the program that way.

You could also run `make messages.pot` to just generate the template file, but this will be done automatically by the previously described rule.

Manual page should also be translated; it is located in `man` project subdirectory.
