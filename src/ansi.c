/*
 * Copyright (C) 2019-2020 Nikola Hadžić
 *
 * This file is part of tttt.
 *
 * tttt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tttt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tttt.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include "const.h"

int tttt_ansi_code(int fd, int code, ...)
{
    int success = TTTT_RETURN_CODE_SUCCESS;

    va_list arg_list;
    va_start(arg_list, code);

    char ansi_code[TTTT_BUFF_SIZE_ANSI_CODE];

    switch (code)
    {
        case TTTT_ANSI_CODE_CLEAR_SCREEN:
            snprintf(ansi_code, TTTT_BUFF_SIZE_ANSI_CODE * sizeof(char), "\033[%dJ", va_arg(arg_list, int));
            break;
        case TTTT_ANSI_CODE_CURSOR_HIDE:
            snprintf(ansi_code, TTTT_BUFF_SIZE_ANSI_CODE * sizeof(char), "\033[?25l");
            break;
        case TTTT_ANSI_CODE_CURSOR_UNHIDE:
            snprintf(ansi_code, TTTT_BUFF_SIZE_ANSI_CODE * sizeof(char), "\033[?25h");
            break;
        case TTTT_ANSI_CODE_ERASE_LINE:
            snprintf(ansi_code, TTTT_BUFF_SIZE_ANSI_CODE * sizeof(char), "\033[%dK", va_arg(arg_list, int));
            break;
        case TTTT_ANSI_CODE_TEXT_ATTR:
            snprintf(ansi_code, TTTT_BUFF_SIZE_ANSI_CODE * sizeof(char), "\033[%dm", va_arg(arg_list, int));
            break;
        case TTTT_ANSI_CODE_CURSOR_UP:
            snprintf(ansi_code, TTTT_BUFF_SIZE_ANSI_CODE * sizeof(char), "\033[%dA", va_arg(arg_list, int));
            break;
        case TTTT_ANSI_CODE_CURSOR_DOWN:
            snprintf(ansi_code, TTTT_BUFF_SIZE_ANSI_CODE * sizeof(char), "\033[%dB", va_arg(arg_list, int));
            break;
        case TTTT_ANSI_CODE_CURSOR_LEFT:
            snprintf(ansi_code, TTTT_BUFF_SIZE_ANSI_CODE * sizeof(char), "\033[%dD", va_arg(arg_list, int));
            break;
        case TTTT_ANSI_CODE_CURSOR_RIGHT:
            snprintf(ansi_code, TTTT_BUFF_SIZE_ANSI_CODE * sizeof(char), "\033[%dC", va_arg(arg_list, int));
            break;
        case TTTT_ANSI_CODE_CURSOR_NEXT:
            snprintf(ansi_code, TTTT_BUFF_SIZE_ANSI_CODE * sizeof(char), "\033[%dE", va_arg(arg_list, int));
            break;
        case TTTT_ANSI_CODE_CURSOR_PREV:
            snprintf(ansi_code, TTTT_BUFF_SIZE_ANSI_CODE * sizeof(char), "\033[%dF", va_arg(arg_list, int));
            break;
        case TTTT_ANSI_CODE_CURSOR_POS:
            snprintf(ansi_code, TTTT_BUFF_SIZE_ANSI_CODE * sizeof(char), "\033[%d;%dH", va_arg(arg_list, int), va_arg(arg_list, int));
            break;
        case TTTT_ANSI_CODE_SCROLL_UP:
            snprintf(ansi_code, TTTT_BUFF_SIZE_ANSI_CODE * sizeof(char), "\033[%dS", va_arg(arg_list, int));
            break;
        case TTTT_ANSI_CODE_SCROLL_DOWN:
            snprintf(ansi_code, TTTT_BUFF_SIZE_ANSI_CODE * sizeof(char), "\033[%dT", va_arg(arg_list, int));
            break;
        case TTTT_ANSI_CODE_CURSOR_SAVE:
            snprintf(ansi_code, TTTT_BUFF_SIZE_ANSI_CODE * sizeof(char), "\033[s");
            break;
        case TTTT_ANSI_CODE_CURSOR_RESTORE:
            snprintf(ansi_code, TTTT_BUFF_SIZE_ANSI_CODE * sizeof(char), "\033[u");
            break;
    }

    if (write(fd, ansi_code, strlen(ansi_code)) < 0)
        success = TTTT_RETURN_CODE_WRITE_FAIL;

    va_end(arg_list);

    return success;
}
