/*
 * Copyright (C) 2019-2021 Nikola Hadžić
 *
 * This file is part of tttt.
 *
 * tttt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tttt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tttt.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <getopt.h>
#include <string.h>
#include "const.h"
#include "elements.h"
#include "msg.h"

int tttt_arg_parser(int argc, char *argv[], struct TTTTOptions * const options)
{
    const struct option longopts[] =  {
                                          {"help",    no_argument,        NULL,   'h'},
                                          {"version", no_argument,        NULL,   'v'},
                                          {"rounds",  required_argument,  NULL,   'r'},
                                          {"time",    required_argument,  NULL,   't'},
                                          {0,         0,                  0,      0  }
                                      };

    optind = 1;
    opterr = 0;

    int getopt_ret;
    while ((getopt_ret = getopt_long(argc, argv, "hvr:t:", longopts, NULL)) != -1)
    {
        char *endptr;

        switch (getopt_ret)
        {
            case 'h':
                options->help = 1;
                break;
            case 'v':
                options->version = 1;
                break;
            case 'r':
                if ((options->rounds = strtol(optarg, &endptr, 10)) < 0 || *endptr != '\0')
                {
                    note = strdup("r");
                    return TTTT_RETURN_CODE_INVALID_ARG;
                }
                break;
            case 't':
                if ((options->time = strtol(optarg, &endptr, 10)) < 0 || *endptr != '\0')
                {
                    note = strdup("t");
                    return TTTT_RETURN_CODE_INVALID_ARG;
                }
                break;
            case '?':
                if ((note = calloc(2, sizeof(char))))
                    note[0] = optopt;

                return TTTT_RETURN_CODE_INVALID_ARG;
        }
    }

    if (argc > optind)
    {
        note = strdup(argv[optind]);
        return TTTT_RETURN_CODE_INVALID_ARG;
    }

    return TTTT_RETURN_CODE_SUCCESS;
}
