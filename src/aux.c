/*
 * Copyright (C) 2021 Nikola Hadžić
 *
 * This file is part of tttt.
 *
 * tttt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tttt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tttt.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <utf8proc.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "gettext.h"

utf8proc_ssize_t tttt_aux_get_utf8_string_length(const utf8proc_uint8_t * const string)
{
    const utf8proc_ssize_t bytes = strlen((const char *) string);
    utf8proc_ssize_t chars = 0;

    utf8proc_size_t char_bytes;
    utf8proc_int32_t current_char;
    for (utf8proc_ssize_t i = 0;(char_bytes = utf8proc_iterate(string + i, bytes - i, &current_char)) > 0;i += char_bytes)
        chars++;

    if (char_bytes < 0)
    {
        fputs(_("Invalid UTF-8 string."), stderr);
        abort();
    }

    return chars;
}
