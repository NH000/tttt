/*
 * Copyright (C) 2019-2020 Nikola Hadžić
 *
 * This file is part of tttt.
 *
 * tttt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tttt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tttt.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <math.h>
#include "const.h"
#include "ansi.h"

void tttt_board_init(struct TTTTBoard * const board, const struct TTTTTerminal * const term)
{
    static bool init = true;

    board->dim_height = term->dim.ws_row - 3 - ((term->dim.ws_row - 3) % 3);
    board->dim_width = term->dim.ws_col - 2 - ((term->dim.ws_col - 2) % 3);

    board->field_height = board->dim_height / 3 - 1;
    board->field_width = board->dim_width / 3 - 1;

    if (init)
    {
        for (unsigned short int i = 0;i < 3;i++)
            memset(board->fields[i], ' ', 3 * sizeof(char));

        board->selected[0] = 0;
        board->selected[1] = 0;
        init = false;
    }

    board->start[0] = term->dim.ws_row - board->dim_height - floor((term->dim.ws_row - board->dim_height) / 2);
    board->start[1] = term->dim.ws_col - board->dim_width - floor((term->dim.ws_col - board->dim_width) / 2);
}

int tttt_board_set_field(const struct TTTTBoard * const board, const struct TTTTTerminal * const term, const coor_t field)
{
    const size_t pos_y = board->start[0] + field[0] * (board->dim_height / 3) + ceil((double) board->field_height / 2.0);
    const size_t pos_x = board->start[1] + field[1] * (board->dim_width / 3) + ceil((double) board->field_width / 2.0);

    if (tttt_ansi_code(term->fd, TTTT_ANSI_CODE_CURSOR_POS, pos_x, pos_y))
        return TTTT_RETURN_CODE_WRITE_FAIL;

    return TTTT_RETURN_CODE_SUCCESS;
}

int tttt_board_draw(const struct TTTTBoard * const board, const struct TTTTTerminal * const term)
{
    if (tttt_ansi_code(term->fd, TTTT_ANSI_CODE_TEXT_ATTR, 7))
        return TTTT_RETURN_CODE_WRITE_FAIL;

    for (size_t h = board->start[0];h <= board->dim_height + board->start[0];h++)
    {
        for (size_t w = board->start[1];w <= board->dim_width + board->start[1];w++)
        {
            if ((h - board->start[0]) % (board->dim_height / 3) == 0 || (w - board->start[1]) % (board->dim_width / 3) == 0)
                if (tttt_ansi_code(term->fd, TTTT_ANSI_CODE_CURSOR_POS, w, h) || write(term->fd, " ", 1) < 0)
                    return TTTT_RETURN_CODE_WRITE_FAIL;
        }
    }

    if (tttt_ansi_code(term->fd, TTTT_ANSI_CODE_TEXT_ATTR, 27))
        return TTTT_RETURN_CODE_WRITE_FAIL;

    for (size_t h = 0;h < 3;h++)
    {
        for (size_t w = 0;w < 3;w++)
        {
            coor_t pos = { h, w };

            if (tttt_board_set_field(board, term, pos) || write(term->fd, &(board->fields[h][w]), 1) < 0)
                return TTTT_RETURN_CODE_WRITE_FAIL;
        }
    }

    if (tttt_board_set_field(board, term, board->selected))
        return TTTT_RETURN_CODE_WRITE_FAIL;

    return TTTT_RETURN_CODE_SUCCESS;
}

int tttt_board_test_for_match(struct TTTTBoard * const board)
{
    for (unsigned short int i = 0;i < 3;i++)
        if (board->fields[i][0] != ' ' && board->fields[i][0] == board->fields[i][1] && board->fields[i][1] == board->fields[i][2])
        {
            switch (board->fields[i][0])
            {
                case 'X':
                    return TTTT_MATCH_CASE_X_WON;
                case 'O':
                    return TTTT_MATCH_CASE_O_WON;
            }
        }

    for (unsigned short int i = 0;i < 3;i++)
        if (board->fields[0][i] != ' ' && board->fields[0][i] == board->fields[1][i] && board->fields[1][i] == board->fields[2][i])
        {
            switch (board->fields[0][i])
            {
                case 'X':
                    return TTTT_MATCH_CASE_X_WON;
                case 'O':
                    return TTTT_MATCH_CASE_O_WON;
            }
        }

    if ((board->fields[0][0] != ' ' && board->fields[0][0] == board->fields[1][1] && board->fields[1][1] == board->fields[2][2]) || (board->fields[0][2] != ' ' && board->fields[0][2] == board->fields[1][1] && board->fields[1][1] == board->fields[2][0]))
    {
            switch (board->fields[1][1])
            {
                case 'X':
                    return TTTT_MATCH_CASE_X_WON;
                case 'O':
                    return TTTT_MATCH_CASE_O_WON;
            }
    }

    for (unsigned short int i = 0;i < 3;i++)
        for (unsigned short int j = 0;j < 3;j++)
            if (board->fields[i][j] == ' ')
                return TTTT_MATCH_CASE_NO_MATCH;

    return TTTT_MATCH_CASE_DRAW;
}
