/*
 * Copyright (C) 2019-2020 Nikola Hadžić
 *
 * This file is part of tttt.
 *
 * tttt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tttt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tttt.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TTTT_BOARD_H
#define TTTT_BOARD_H
#include <stdbool.h>
#include "const.h"

void tttt_board_init(struct TTTTBoard * const board, const struct TTTTTerminal * const term);
int tttt_board_draw(const struct TTTTBoard * const board, const struct TTTTTerminal * const term);
int tttt_board_set_field(const struct TTTTBoard * const board, const struct TTTTTerminal * const term, const coor_t field);
int tttt_board_test_for_match(struct TTTTBoard * const board);

#endif
