/*
 * Copyright (C) 2019-2020 Nikola Hadžić
 *
 * This file is part of tttt.
 *
 * tttt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tttt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tttt.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TTTT_CONST_H
#define TTTT_CONST_H
#include <stdbool.h>
#include <stdlib.h>
#include <termios.h>
#include <sys/ioctl.h>
#include "gettext.h"

#define TTTT_TEST_TIME(t) system("sleep "#t)

#define TTTT_PROG_NAME  "tttt"
#define TTTT_PROG_DESC  N_("tic-tac-toe in terminal")
#define TTTT_PROG_VER   "1.0"

typedef unsigned int coor_t[2];

enum BufferSizes {
            TTTT_BUFF_SIZE_GEN = 1024,
            TTTT_BUFF_SIZE_ANSI_CODE = 32,
            TTTT_BUFF_SIZE_KEY_CODE = 7
        };

enum ReturnCodes {
            TTTT_RETURN_CODE_SUCCESS,
            TTTT_RETURN_CODE_NO_MEMORY,
            TTTT_RETURN_CODE_INVALID_ARG,
            TTTT_RETURN_CODE_NOT_TTY,
            TTTT_RETURN_CODE_TERM_GET_ATTR_FAIL,
            TTTT_RETURN_CODE_TERM_SET_ATTR_FAIL,
            TTTT_RETURN_CODE_IOCTL_FAIL,
            TTTT_RETURN_CODE_READ_FAIL,
            TTTT_RETURN_CODE_WRITE_FAIL,
            TTTT_RETURN_CODE_THREAD_CREAT_FAIL,
            TTTT_RETURN_CODE_THREAD_JOIN_FAIL,
            TTTT_RETURN_CODE_FILE_GET_FLAGS_FAIL,
            TTTT_RETURN_CODE_FILE_SET_FLAGS_FAIL
        };

enum AnsiCodes {
            TTTT_ANSI_CODE_CLEAR_SCREEN,
            TTTT_ANSI_CODE_CURSOR_HIDE,
            TTTT_ANSI_CODE_CURSOR_UNHIDE,
            TTTT_ANSI_CODE_ERASE_LINE,
            TTTT_ANSI_CODE_TEXT_ATTR,
            TTTT_ANSI_CODE_CURSOR_UP,
            TTTT_ANSI_CODE_CURSOR_DOWN,
            TTTT_ANSI_CODE_CURSOR_LEFT,
            TTTT_ANSI_CODE_CURSOR_RIGHT,
            TTTT_ANSI_CODE_CURSOR_NEXT,
            TTTT_ANSI_CODE_CURSOR_PREV,
            TTTT_ANSI_CODE_CURSOR_POS,
            TTTT_ANSI_CODE_SCROLL_UP,
            TTTT_ANSI_CODE_SCROLL_DOWN,
            TTTT_ANSI_CODE_CURSOR_SAVE,
            TTTT_ANSI_CODE_CURSOR_RESTORE
        };

enum AnsiCodeClearScreen {
                TTTT_ANSI_CODE_CLEAR_SCREEN_END,
                TTTT_ANSI_CODE_CLEAR_SCREEN_START,
                TTTT_ANSI_CODE_CLEAR_SCREEN_ALL
            };

enum AnsiCodeEraseLine {
                TTTT_ANSI_CODE_ERASE_LINE_END,
                TTTT_ANSI_CODE_ERASE_LINE_START,
                TTTT_ANSI_CODE_ERASE_LINE_ALL
            };

enum MatchCases {
            TTTT_MATCH_CASE_X_WON,
            TTTT_MATCH_CASE_O_WON,
            TTTT_MATCH_CASE_NO_MATCH,
            TTTT_MATCH_CASE_DRAW
        };

struct TTTTOptions
{
    bool help;
    bool version;
    int rounds;
    int time;
};

struct TTTTTerminal
{
    struct termios original;
    int fd;
    char *name;
    struct winsize dim;
};

struct TTTTBoard
{
    size_t dim_height, dim_width;
    size_t field_height, field_width;
    char fields[3][3];
    coor_t start;
    coor_t selected;
};

struct TTTTGame
{
    bool turn;
    int rounds;
    int time;
    int wins[2];
    bool running;
};

struct TTTTElements
{
    struct TTTTOptions *options;
    struct TTTTTerminal *term_in, *term_out;
    struct TTTTBoard *board;
    struct TTTTGame *game;
};

#endif
