/*
 * Copyright (C) 2019-2020 Nikola Hadžić
 *
 * This file is part of tttt.
 *
 * tttt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tttt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tttt.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include "const.h"

#define TTTT_ELEMENT_CREATE(element, type)\
    if (((element) = calloc(1, sizeof(type))) == NULL)\
        return NULL;

void tttt_elements_destroy(struct TTTTElements * const elements)
{
    if (elements != NULL)
    {
        if (elements->game != NULL)
        {
            free(elements->game);
        }

        if (elements->board != NULL)
        {
            free(elements->board);
        }

        if (elements->term_out != NULL)
        {
            free(elements->term_out->name);
            free(elements->term_out);
        }
        if (elements->term_in != NULL)
        {
            free(elements->term_in->name);
            free(elements->term_in);
        }

        if (elements->options != NULL)
        {
            free(elements->options);
        }

        free(elements);
    }
}

struct TTTTElements *tttt_elements_create()
{
    struct TTTTElements *elements;

    TTTT_ELEMENT_CREATE(elements, struct TTTTElements);
    TTTT_ELEMENT_CREATE(elements->options, struct TTTTOptions);
    TTTT_ELEMENT_CREATE(elements->term_in, struct TTTTTerminal);
    TTTT_ELEMENT_CREATE(elements->term_out, struct TTTTTerminal);
    TTTT_ELEMENT_CREATE(elements->board, struct TTTTBoard);
    TTTT_ELEMENT_CREATE(elements->game, struct TTTTGame);

    return elements;
}
