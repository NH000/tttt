/*
 * Copyright (C) 2019-2020 Nikola Hadžić
 *
 * This file is part of tttt.
 *
 * tttt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tttt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tttt.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <fcntl.h>
#include <errno.h>
#include "const.h"
#include "board.h"
#include "msg.h"
#include "status.h"
#include "gettext.h"

int tttt_input_accept(const struct TTTTTerminal * const term_in, const struct TTTTTerminal * const term_out, struct TTTTBoard * const board, struct TTTTGame * const game, const struct TTTTOptions * const options)
{
    char key[TTTT_BUFF_SIZE_KEY_CODE];

    int term_in_flags = fcntl(term_in->fd, F_GETFL);
    if (term_in_flags == -1)
    {
        note = strdup("STDIN");
        return TTTT_RETURN_CODE_FILE_GET_FLAGS_FAIL;
    }

    if (fcntl(term_in->fd, F_SETFL, term_in_flags | O_NONBLOCK) == -1)
    {
        note = strdup("STDIN");
        return TTTT_RETURN_CODE_FILE_SET_FLAGS_FAIL;
    }

    while (game->running)
    {
        memset(key, '\0', TTTT_BUFF_SIZE_KEY_CODE * sizeof(char));

        if (read(term_in->fd, key, TTTT_BUFF_SIZE_KEY_CODE * sizeof(char)) < 0)
        {
            if (errno == EAGAIN)
                continue;

            note = strdup("STDIN");
            return TTTT_RETURN_CODE_READ_FAIL;
        }

        if (!strcmp(key, _("h")) || !strcmp(key, "\033[D"))
        {
            if (board->selected[1] > 0)
                board->selected[1]--;

            if (tttt_board_set_field(board, term_out, board->selected))
            {
                note = strdup("STDOUT");
                return TTTT_RETURN_CODE_WRITE_FAIL;
            }
        }
        else if (!strcmp(key, _("l")) || !strcmp(key, "\033[C"))
        {
            if (board->selected[1] < 2)
                board->selected[1]++;

            if (tttt_board_set_field(board, term_out, board->selected))
            {
                note = strdup("STDOUT");
                return TTTT_RETURN_CODE_WRITE_FAIL;
            }
        }
        else if (!strcmp(key, _("j")) || !strcmp(key, "\033[B"))
        {
            if (board->selected[0] < 2)
                board->selected[0]++;

            if (tttt_board_set_field(board, term_out, board->selected))
            {
                note = strdup("STDOUT");
                return TTTT_RETURN_CODE_WRITE_FAIL;
            }
        }
        else if (!strcmp(key, _("k")) || !strcmp(key, "\033[A"))
        {
            if (board->selected[0] > 0)
                board->selected[0]--;

            if (tttt_board_set_field(board, term_out, board->selected))
            {
                note = strdup("STDOUT");
                return TTTT_RETURN_CODE_WRITE_FAIL;
            }
        }
        else if (!strcmp(key, "\r") || !strcmp(key, " "))
        {
            if (board->fields[board->selected[0]][board->selected[1]] == ' ')
            {
                char c = game->turn ? 'O' : 'X';

                if (write(term_out->fd, &c, 1) < 0 || tttt_board_set_field(board, term_out, board->selected))
                {
                    note = strdup("STDOUT");
                    return TTTT_RETURN_CODE_WRITE_FAIL;
                }

                board->fields[board->selected[0]][board->selected[1]] = c;
                game->turn = !game->turn;

                switch (tttt_board_test_for_match(board))
                {
                    case TTTT_MATCH_CASE_X_WON:
                        game->wins[0]++;
                        game->rounds++;

                        if (game->rounds == options->rounds)
                        {
                            game->running = false;
                            break;
                        }

                        for (unsigned short int i = 0;i < 3;i++)
                            memset(board->fields[i], ' ', 3 * sizeof(char));

                        game->turn = false;
                        board->selected[0] = 0;
                        board->selected[1] = 0;

                        if (tttt_board_draw(board, term_out))
                        {
                            note = strdup("STDOUT");
                            return TTTT_RETURN_CODE_WRITE_FAIL;
                        }

                        switch (tttt_status_draw(game, term_out, options))
                        {
                            case TTTT_RETURN_CODE_NO_MEMORY:
                                return TTTT_RETURN_CODE_NO_MEMORY;
                            case TTTT_RETURN_CODE_WRITE_FAIL:
                                note = strdup("STDOUT");
                                return TTTT_RETURN_CODE_WRITE_FAIL;
                        }
                        break;
                    case TTTT_MATCH_CASE_O_WON:
                        game->wins[1]++;
                        game->rounds++;

                        if (game->rounds == options->rounds)
                        {
                            game->running = false;
                            break;
                        }

                        for (unsigned short int i = 0;i < 3;i++)
                            memset(board->fields[i], ' ', 3 * sizeof(char));

                        game->turn = false;
                        board->selected[0] = 0;
                        board->selected[1] = 0;

                        if (tttt_board_draw(board, term_out))
                        {
                            note = strdup("STDOUT");
                            return TTTT_RETURN_CODE_WRITE_FAIL;
                        }

                        switch (tttt_status_draw(game, term_out, options))
                        {
                            case TTTT_RETURN_CODE_NO_MEMORY:
                                return TTTT_RETURN_CODE_NO_MEMORY;
                            case TTTT_RETURN_CODE_WRITE_FAIL:
                                note = strdup("STDOUT");
                                return TTTT_RETURN_CODE_WRITE_FAIL;
                        }
                        break;
                    case TTTT_MATCH_CASE_DRAW:
                        game->rounds++;

                        if (game->rounds == options->rounds)
                        {
                            game->running = false;
                            break;
                        }

                        for (unsigned short int i = 0;i < 3;i++)
                            memset(board->fields[i], ' ', 3 * sizeof(char));

                        game->turn = false;
                        board->selected[0] = 0;
                        board->selected[1] = 0;

                        if (tttt_board_draw(board, term_out))
                        {
                            note = strdup("STDOUT");
                            return TTTT_RETURN_CODE_WRITE_FAIL;
                        }

                        switch (tttt_status_draw(game, term_out, options))
                        {
                            case TTTT_RETURN_CODE_NO_MEMORY:
                                return TTTT_RETURN_CODE_NO_MEMORY;
                            case TTTT_RETURN_CODE_WRITE_FAIL:
                                note = strdup("STDOUT");
                                return TTTT_RETURN_CODE_WRITE_FAIL;
                        }
                        break;
                }
            }
        }
        else if (!strcmp(key, _("q")) || !strcmp(key, "\033"))
            game->running = false;
    }

    if (fcntl(term_in->fd, F_SETFL, term_in_flags) == -1)
    {
        note = strdup("STDIN");
        return TTTT_RETURN_CODE_FILE_SET_FLAGS_FAIL;
    }

    return TTTT_RETURN_CODE_SUCCESS;
}
