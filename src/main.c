/*
 * Copyright (C) 2019-2021 Nikola Hadžić
 *
 * This file is part of tttt.
 *
 * tttt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tttt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tttt.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <locale.h>
#include <libintl.h>
#include "elements.h"
#include "const.h"
#include "gettext.h"
#include "msg.h"
#include "arg.h"
#include "term.h"
#include "board.h"
#include "signal.h"
#include "ansi.h"
#include "input.h"
#include "status.h"
#include "timer.h"

#define TTTT_EXIT_ON_ERROR(err) { tttt_msg_error(err); goto end; }

int main(int argc, char *argv[])
{
    setlocale(LC_ALL, "");
    bindtextdomain(TTTT_GETTEXT_TEXT_DOMAIN, TTTT_GETTEXT_TEXT_DOMAIN_DIR);
    textdomain(TTTT_GETTEXT_TEXT_DOMAIN);

    int success = TTTT_RETURN_CODE_SUCCESS;

    struct TTTTElements *elements;

    if (!(elements = tttt_elements_create()))
    {
        success = TTTT_RETURN_CODE_NO_MEMORY;
        TTTT_EXIT_ON_ERROR(success);
    }

    if ((success = tttt_arg_parser(argc, argv, elements->options)))
        TTTT_EXIT_ON_ERROR(success);

    if (elements->options->help)
    {
        tttt_msg_help();
        goto end;
    }
    else if (elements->options->version)
    {
        tttt_msg_version();
        goto end;
    }

    if ((success = tttt_term_init(elements->term_in, STDIN_FILENO)))
    {
        note = strdup("STDIN");
        TTTT_EXIT_ON_ERROR(success);
    }

    if ((success = tttt_term_init(elements->term_out, STDOUT_FILENO)))
    {
        note = strdup("STDOUT");
        tttt_term_restore(elements->term_in);
        TTTT_EXIT_ON_ERROR(success);
    }

    if ((success = tttt_term_mode_cbreak(elements->term_in)))
    {
        note = strdup("STDIN");
        tttt_term_restore(elements->term_in);
        tttt_term_restore(elements->term_out);
        TTTT_EXIT_ON_ERROR(success);
    }

    tttt_signal_connect(elements);

    tttt_board_init(elements->board, elements->term_out);

    if ((success = tttt_ansi_code(elements->term_out->fd, TTTT_ANSI_CODE_CLEAR_SCREEN, TTTT_ANSI_CODE_CLEAR_SCREEN_ALL)) || (success = tttt_board_draw(elements->board, elements->term_out)))
    {
        note = strdup("STDOUT");
        tttt_term_restore(elements->term_in);
        tttt_term_restore(elements->term_out);
        TTTT_EXIT_ON_ERROR(success);
    }

    if ((success = tttt_status_init()) || (success = tttt_status_draw(elements->game, elements->term_out, elements->options)))
    {
        note = strdup("STDOUT");
        tttt_status_destroy();
        tttt_term_restore(elements->term_in);
        tttt_term_restore(elements->term_out);
        TTTT_EXIT_ON_ERROR(success);
    }

    elements->game->running = true;

    if ((success = tttt_timer_start(elements)))
    {
        tttt_status_destroy();
        tttt_term_restore(elements->term_in);
        tttt_term_restore(elements->term_out);
        TTTT_EXIT_ON_ERROR(success);
    }

    if ((success = tttt_input_accept(elements->term_in, elements->term_out, elements->board, elements->game, elements->options)))
    {
        tttt_timer_stop();
        tttt_status_destroy();
        tttt_term_restore(elements->term_in);
        tttt_term_restore(elements->term_out);
        TTTT_EXIT_ON_ERROR(success);
    }

    if ((success = tttt_timer_stop()))
    {
        tttt_status_destroy();
        tttt_term_restore(elements->term_in);
        tttt_term_restore(elements->term_out);
        TTTT_EXIT_ON_ERROR(success);
    }

    tttt_status_destroy();

    if ((success = tttt_term_restore(elements->term_in)))
    {
        note = strdup("STDIN");
        tttt_term_restore(elements->term_out);
        TTTT_EXIT_ON_ERROR(success);
    }

    if ((success = tttt_term_restore(elements->term_out)))
    {
        note = strdup("STDOUT");
        TTTT_EXIT_ON_ERROR(success);
    }

    if ((success = tttt_ansi_code(elements->term_out->fd, TTTT_ANSI_CODE_CLEAR_SCREEN, TTTT_ANSI_CODE_CLEAR_SCREEN_ALL)) || (success = tttt_ansi_code(elements->term_out->fd, TTTT_ANSI_CODE_CURSOR_POS, 1, 1)))
    {
        note = strdup("STDOUT");
        TTTT_EXIT_ON_ERROR(success);
    }

    tttt_msg_results(elements->options, elements->game);

end:
    tttt_elements_destroy(elements);
    return success;
}
