/*
 * Copyright (C) 2019-2021 Nikola Hadžić
 *
 * This file is part of tttt.
 *
 * tttt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tttt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tttt.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "const.h"
#include "gettext.h"

char *note;

void tttt_msg_results(const struct TTTTOptions * const options, const struct TTTTGame * const game)
{
    puts(_("TTTT RESULTS:"));

    fputs(_("Status: "), stdout);

    if (game->wins[0] > game->wins[1])
        fputs(_("X won"), stdout);
    else if (game->wins[0] < game->wins[1])
        fputs(_("O won"), stdout);
    else
        fputs(_("Draw"), stdout);

    fputc('\n', stdout);

    printf(_("Points: X: %d, O: %d\n"), game->wins[0], game->wins[1]);

    fputs(_("Rounds played: "), stdout);
    printf("%d", game->rounds);

    if (options->rounds)
        printf("/%d", options->rounds);

    fputc('\n', stdout);

    fputs(_("Time played: "), stdout);
    printf("%d", game->time);

    if (options->time)
        printf("/%d", options->time);

    fputc('\n', stdout);
}

void tttt_msg_help()
{
    printf(TTTT_PROG_NAME " - %s\n\n", gettext(TTTT_PROG_DESC));
    printf(_("USAGE:\n"));
    printf("\t%s [-h] [-v] [-r UINT] [-t UINT]\n\n", TTTT_PROG_NAME);
    printf(_("OPTIONS:\n"));
    printf("\t%-30s%s\n", "-h, --help", _("show this help menu"));
    printf("\t%-30s%s\n", "-v, --version", _("display program version"));
    printf("\t%-30s%s\n", "-r UINT, --rounds UINT", _("limit on the number of rounds"));
    printf("\t%-30s%s\n", "-t UINT, --time UINT", _("time limit in seconds"));
}

void tttt_msg_version()
{
    puts(TTTT_PROG_VER);
}

void tttt_msg_error(int error_code)
{
    fputs(_("Error: "), stderr);

    switch (error_code)
    {
        case TTTT_RETURN_CODE_NO_MEMORY:
            fprintf(stderr, _("Failed to allocate memory."));
            break;
        case TTTT_RETURN_CODE_INVALID_ARG:
            fprintf(stderr, _("Invalid command-line argument '%s'."), note);
            break;
        case TTTT_RETURN_CODE_NOT_TTY:
            fprintf(stderr, _("%s is not a TTY."), note);
            break;
        case TTTT_RETURN_CODE_TERM_GET_ATTR_FAIL:
            fprintf(stderr, _("Failed to get attributes of %s."), note);
            break;
        case TTTT_RETURN_CODE_TERM_SET_ATTR_FAIL:
            fprintf(stderr, _("Failed to set attributes of %s."), note);
            break;
        case TTTT_RETURN_CODE_IOCTL_FAIL:
            fprintf(stderr, _("Call to ioctl for %s failed."), note);
            break;
        case TTTT_RETURN_CODE_READ_FAIL:
            fprintf(stderr, _("Reading from %s failed."), note);
            break;
        case TTTT_RETURN_CODE_WRITE_FAIL:
            fprintf(stderr, _("Writing to %s failed."), note);
            break;
        case TTTT_RETURN_CODE_THREAD_CREAT_FAIL:
            fprintf(stderr, _("Failed to create a thread."));
            break;
        case TTTT_RETURN_CODE_THREAD_JOIN_FAIL:
            fprintf(stderr, _("Failed to join a thread."));
            break;
        case TTTT_RETURN_CODE_FILE_GET_FLAGS_FAIL:
            fprintf(stderr, _("Failed to get flags of file %s."), note);
            break;
        case TTTT_RETURN_CODE_FILE_SET_FLAGS_FAIL:
            fprintf(stderr, _("Failed to set flags of file %s."), note);
            break;
        default:
            fprintf(stderr, _("Unknown error."));
    }

    fputc('\n', stderr);

    free(note);
    note = NULL;
}
