/*
 * Copyright (C) 2019-2020 Nikola Hadžić
 *
 * This file is part of tttt.
 *
 * tttt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tttt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tttt.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TTTT_MSG_H
#define TTTT_MSG_H
#include "const.h"

extern char *note;

void tttt_msg_results(const struct TTTTOptions * const options, const struct TTTTGame * const game);
void tttt_msg_help();
void tttt_msg_version();
void tttt_msg_error();

#endif
