/*
 * Copyright (C) 2019-2020 Nikola Hadžić
 *
 * This file is part of tttt.
 *
 * tttt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tttt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tttt.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <signal.h>
#include "const.h"
#include "term.h"
#include "board.h"
#include "ansi.h"
#include "status.h"

static struct TTTTElements *elements;

static void tttt_signal_handler(int sig)
{
    switch (sig)
    {
        case SIGWINCH:
            tttt_term_get_size(elements->term_out);
            tttt_ansi_code(elements->term_out->fd, TTTT_ANSI_CODE_CLEAR_SCREEN, TTTT_ANSI_CODE_CLEAR_SCREEN_ALL);
            tttt_board_init(elements->board, elements->term_out);
            tttt_board_draw(elements->board, elements->term_out);
            tttt_status_draw(elements->game, elements->term_out, elements->options);
            break;
    }
}

void tttt_signal_connect(struct TTTTElements * _elements)
{
    elements = _elements;

    signal(SIGWINCH, tttt_signal_handler);
}
