/*
 * Copyright (C) 2019-2021 Nikola Hadžić
 *
 * This file is part of tttt.
 *
 * tttt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tttt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tttt.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "const.h"
#include "ansi.h"
#include "aux.h"
#include "gettext.h"

#define TTTT_STATUS_ROUNDS      N_("Round: ")
#define TTTT_STATUS_TIME        N_("Time: ")
#define TTTT_STATUS_SEPARATOR   " | "
#define TTTT_STATUS_DELIMITER   "/"
#define TTTT_STATUS_X_WON       "X: "
#define TTTT_STATUS_O_WON       "O: "

struct StatusBar
{
    char *rounds;
    char *time;
    char *x_won;
    char *o_won;
    char *rounds_total;
    char *time_total;
    char *part_one;
    char *part_two;
};

static struct StatusBar status_bar;

int tttt_status_init()
{
    status_bar.rounds = malloc(TTTT_BUFF_SIZE_GEN * sizeof(char));
    if (status_bar.rounds == NULL)
        return TTTT_RETURN_CODE_NO_MEMORY;
    status_bar.time = malloc(TTTT_BUFF_SIZE_GEN * sizeof(char));
    if (status_bar.time == NULL)
        return TTTT_RETURN_CODE_NO_MEMORY;
    status_bar.x_won = malloc(TTTT_BUFF_SIZE_GEN * sizeof(char));
    if (status_bar.x_won == NULL)
        return TTTT_RETURN_CODE_NO_MEMORY;
    status_bar.o_won = malloc(TTTT_BUFF_SIZE_GEN * sizeof(char));
    if (status_bar.o_won == NULL)
        return TTTT_RETURN_CODE_NO_MEMORY;
    status_bar.rounds_total = malloc(TTTT_BUFF_SIZE_GEN * sizeof(char));
    if (status_bar.rounds_total == NULL)
        return TTTT_RETURN_CODE_NO_MEMORY;
    status_bar.time_total = malloc(TTTT_BUFF_SIZE_GEN * sizeof(char));
    if (status_bar.time_total == NULL)
        return TTTT_RETURN_CODE_NO_MEMORY;

    return TTTT_RETURN_CODE_SUCCESS;
}

void tttt_status_destroy()
{
    free(status_bar.part_two);
    free(status_bar.part_one);
    free(status_bar.time_total);
    free(status_bar.rounds_total);
    free(status_bar.o_won);
    free(status_bar.x_won);
    free(status_bar.time);
    free(status_bar.rounds);
}

int tttt_status_draw(const struct TTTTGame * const game, const struct TTTTTerminal * const term, const struct TTTTOptions * const options)
{
    snprintf(status_bar.rounds, TTTT_BUFF_SIZE_GEN * sizeof(char), "%d", game->rounds + 1);
    snprintf(status_bar.time, TTTT_BUFF_SIZE_GEN * sizeof(char), "%d", game->time + 1);
    snprintf(status_bar.x_won, TTTT_BUFF_SIZE_GEN * sizeof(char), "%d", game->wins[0]);
    snprintf(status_bar.o_won, TTTT_BUFF_SIZE_GEN * sizeof(char), "%d", game->wins[1]);
    snprintf(status_bar.rounds_total, TTTT_BUFF_SIZE_GEN * sizeof(char), "%d", options->rounds);
    snprintf(status_bar.time_total, TTTT_BUFF_SIZE_GEN * sizeof(char), "%d", options->time);

    size_t part_one_s = strlen(gettext(TTTT_STATUS_ROUNDS)) + strlen(status_bar.rounds) + (sizeof(TTTT_STATUS_SEPARATOR) - 1) + strlen(gettext(TTTT_STATUS_TIME)) + strlen(status_bar.time);
    size_t part_two_s = (sizeof(TTTT_STATUS_X_WON) - 1) + strlen(status_bar.x_won) + (sizeof(TTTT_STATUS_SEPARATOR) - 1) + (sizeof(TTTT_STATUS_O_WON) - 1) + strlen(status_bar.o_won);

    if (options->rounds)
        part_one_s = part_one_s + (sizeof(TTTT_STATUS_DELIMITER) - 1) + strlen(status_bar.rounds_total);

    if (options->time)
        part_one_s = part_one_s + (sizeof(TTTT_STATUS_DELIMITER) - 1) + strlen(status_bar.time_total);

    free(status_bar.part_one);
    free(status_bar.part_two);

    if ((status_bar.part_one = malloc((part_one_s + 1) * sizeof(char))) == NULL || (status_bar.part_two = malloc((part_two_s + 1) * sizeof(char))) == NULL)
        return TTTT_RETURN_CODE_NO_MEMORY;

    strcpy(status_bar.part_one, gettext(TTTT_STATUS_ROUNDS));
    strcat(status_bar.part_one, status_bar.rounds);
    if (options->rounds)
    {
        strcat(status_bar.part_one, TTTT_STATUS_DELIMITER);
        strcat(status_bar.part_one, status_bar.rounds_total);
    }
    strcat(status_bar.part_one, TTTT_STATUS_SEPARATOR);
    strcat(status_bar.part_one, gettext(TTTT_STATUS_TIME));
    strcat(status_bar.part_one, status_bar.time);
    if (options->time)
    {
        strcat(status_bar.part_one, TTTT_STATUS_DELIMITER);
        strcat(status_bar.part_one, status_bar.time_total);
    }

    strcpy(status_bar.part_two, TTTT_STATUS_X_WON);
    strcat(status_bar.part_two, status_bar.x_won);
    strcat(status_bar.part_two, TTTT_STATUS_SEPARATOR);
    strcat(status_bar.part_two, TTTT_STATUS_O_WON);
    strcat(status_bar.part_two, status_bar.o_won);

    if (tttt_ansi_code(term->fd, TTTT_ANSI_CODE_CURSOR_SAVE) || tttt_ansi_code(term->fd, TTTT_ANSI_CODE_CURSOR_POS, 1, term->dim.ws_row) || tttt_ansi_code(term->fd, TTTT_ANSI_CODE_TEXT_ATTR, 7))
        return TTTT_RETURN_CODE_WRITE_FAIL;

    if (write(term->fd, status_bar.part_one, part_one_s) < 0)
        return TTTT_RETURN_CODE_WRITE_FAIL;

    {
        const utf8proc_ssize_t part_one_l = tttt_aux_get_utf8_string_length((const utf8proc_uint8_t *) status_bar.part_one);
        const utf8proc_ssize_t part_two_l = tttt_aux_get_utf8_string_length((const utf8proc_uint8_t *) status_bar.part_two);

        for (size_t i = 0;i < term->dim.ws_col - (part_one_l + part_two_l);i++)
            if (write(term->fd, " ", 1) < 0)
                return TTTT_RETURN_CODE_WRITE_FAIL;
    }

    if (write(term->fd, status_bar.part_two, part_two_s) < 0)
        return TTTT_RETURN_CODE_WRITE_FAIL;

    if (tttt_ansi_code(term->fd, TTTT_ANSI_CODE_TEXT_ATTR, 27) || tttt_ansi_code(term->fd, TTTT_ANSI_CODE_CURSOR_RESTORE))
        return TTTT_RETURN_CODE_WRITE_FAIL;

    return TTTT_RETURN_CODE_SUCCESS;
}
