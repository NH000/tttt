/*
 * Copyright (C) 2019-2020 Nikola Hadžić
 *
 * This file is part of tttt.
 *
 * tttt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tttt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tttt.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <termios.h>
#include <string.h>
#include <sys/ioctl.h>
#include "const.h"

int tttt_term_mode_cbreak(struct TTTTTerminal * const term)
{
    struct termios attr;
    if (tcgetattr(term->fd, &attr) < 0)
        return TTTT_RETURN_CODE_TERM_GET_ATTR_FAIL;

    attr.c_lflag = attr.c_lflag & ~(ICANON | ECHO);
    attr.c_lflag = attr.c_lflag | ISIG;
    attr.c_iflag = attr.c_iflag & ~ICRNL;
    attr.c_cc[VMIN] = 1;
    attr.c_cc[VTIME] = 0;

    if (tcsetattr(term->fd, TCSAFLUSH, &attr) < 0)
        return TTTT_RETURN_CODE_TERM_SET_ATTR_FAIL;

    return TTTT_RETURN_CODE_SUCCESS;
}

int tttt_term_get_size(struct TTTTTerminal * const term)
{
    if (ioctl(term->fd, TIOCGWINSZ, &(term->dim)) == -1)
        return TTTT_RETURN_CODE_IOCTL_FAIL;

    return TTTT_RETURN_CODE_SUCCESS;
}

int tttt_term_init(struct TTTTTerminal * const term, int fd)
{
    if (!isatty(fd))
        return TTTT_RETURN_CODE_NOT_TTY;

    term->fd = fd;
    term->name = strdup(ttyname(term->fd));

    if (tcgetattr(term->fd, &(term->original)) < 0)
        return TTTT_RETURN_CODE_TERM_GET_ATTR_FAIL;

    if (tttt_term_get_size(term))
        return TTTT_RETURN_CODE_IOCTL_FAIL;

    return TTTT_RETURN_CODE_SUCCESS;
}

int tttt_term_restore(struct TTTTTerminal * const term)
{
    if (tcsetattr(term->fd, TCSAFLUSH, &(term->original)) < 0)
        return TTTT_RETURN_CODE_TERM_SET_ATTR_FAIL;

    return TTTT_RETURN_CODE_SUCCESS;
}
