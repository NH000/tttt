/*
 * Copyright (C) 2019-2020 Nikola Hadžić
 *
 * This file is part of tttt.
 *
 * tttt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tttt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tttt.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TTTT_TERM_H
#define TTTT_TERM_H
#include "const.h"

int tttt_term_mode_cbreak(struct TTTTTerminal * const term);
int tttt_term_get_size(struct TTTTTerminal * const term);
int tttt_term_init(struct TTTTTerminal * const term, int fd);
int tttt_term_restore(struct TTTTTerminal * const term);

#endif
