/*
 * Copyright (C) 2019-2020 Nikola Hadžić
 *
 * This file is part of tttt.
 *
 * tttt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * tttt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with tttt.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <pthread.h>
#include <stdbool.h>
#include <unistd.h>
#include "const.h"
#include "status.h"

#define TTTT_TIMER_INTERVAL 1

static pthread_t threads[1];

static void *tttt_timer_work(void *_elements)
{
    struct TTTTElements *elements = (struct TTTTElements *) _elements;

    while (elements->game->running)
    {
        sleep(TTTT_TIMER_INTERVAL);

        elements->game->time++;
        tttt_status_draw(elements->game, elements->term_out, elements->options);

        if (elements->game->time == elements->options->time)
            elements->game->running = false;
    }

    return NULL;
}

int tttt_timer_start(struct TTTTElements * const elements)
{
    if (pthread_create(&(threads[0]), NULL, tttt_timer_work, elements))
        return TTTT_RETURN_CODE_THREAD_CREAT_FAIL;

    return TTTT_RETURN_CODE_SUCCESS;
}

int tttt_timer_stop()
{
    for (size_t th = 0;th < sizeof(threads[th]) / sizeof(pthread_t);th++)
        if (pthread_join(threads[th], NULL))
            return TTTT_RETURN_CODE_THREAD_JOIN_FAIL;

    return TTTT_RETURN_CODE_SUCCESS;
}
